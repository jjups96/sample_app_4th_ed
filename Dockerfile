FROM ruby:2.5
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /sample_app_4th_ed
WORKDIR /sample_app_4th_ed
COPY Gemfile /sample_app_4th_ed/Gemfile
COPY Gemfile.lock /sample_app_4th_ed/Gemfile.lock
RUN bundle install
COPY . /sample_app_4th_ed
