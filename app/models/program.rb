class Program < ApplicationRecord
  belongs_to :user
  default_scope -> { order(created_at: :desc) }

  validates :name_program,  presence: true, length: { maximum: 50 }
  validates :faculty, length: { maximum: 50 }
  validates :departament, length: { maximum: 50 }
  validates :classroom, length: { maximum: 50 }
  #validates :min_capacity, numericality: true
  #validates :max_capacity, numericality: true
  #validates :instructor, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  #validates :email_instructor, length: { maximum: 255 },format: { with: VALID_EMAIL_REGEX }
  #validates :cv, length: { maximum: 255 }
  #validates :institution_instructor, length: { maximum: 50 }
  validates :resources, length: { maximum: 255 }
  validates :self_financing, length: { maximum: 255 }
  validates :scope, length: { maximum: 255 }

end
