class Student < ApplicationRecord
    validates :name,  presence: true, length: { maximum: 50 }
    validates :institution,  presence: true, length: { maximum: 50 }
end
