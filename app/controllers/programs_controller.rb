class ProgramsController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]

  def index
    @programs = Program.all
  end

  def show
    @program = Program.find(params[:id])
  end

  def new
    @program = Program.new
  end

  def create
    @program = current_user.programs.build(program_params)
    if @program.save
      flash[:success] = "Operacion exitosa"
      redirect_to '/'
    else
      render 'programs/new'
    end
  end

  def edit
    @program = Program.find(params[:id])
  end

  def update
    @program = Program.find(params[:id])
    if @program.update_attributes(program_params)
      flash[:success] = "Profile updated"
      redirect_to current_user
    else
      render 'edit'
    end
  end

  def destroy
    Program.find(params[:id]).destroy
    flash[:success] = "Programa eliminado"
    redirect_to programs_url
  end

  private

    def program_params
      params.require(:program).permit(:name_program, :faculty, :departament,
                                   :classroom, :min_capacity, :max_capacity,
                                   :instructor, :email_instructor, :cv,
                                   :institution_instructor, :resources,
                                   :self_financing, :scope)
    end
end
