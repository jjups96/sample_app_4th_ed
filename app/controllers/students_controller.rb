class StudentsController < ApplicationController
  def new
    @student = Student.new
  end

  def show
    @student = Student.find(params[:id])
  end

  def index
    @students = Student.all
  end

  def create
    @student = Student.new(student_params)
    if @student.save
      flash[:success] = "Operacion exitosa"
      redirect_to '/'
    else
      render 'students/new'
    end
  end

  def edit
    @student = Student.find(params[:id])
  end

  def update
    @student = Student.find(params[:id])
    if @student.update_attributes(student_params)
      flash[:success] = "Profile updated"
      redirect_to students_url
    else
      render 'edit'
    end
  end

  def destroy
    Student.find(params[:id]).destroy
    flash[:success] = "Alumno eliminado"
    redirect_to students_url
  end

  private

    def student_params
      params.require(:student).permit(:name, :institution, :attendance_state)
    end

end
