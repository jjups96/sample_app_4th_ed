class CreatePrograms < ActiveRecord::Migration[5.2]
  def change
    create_table :programs do |t|
      t.string :name_program
      t.date :start_date
      t.date :end_date
      t.string :faculty
      t.string :departament
      t.string :classroom
      t.integer :min_capacity
      t.integer :max_capacity
      t.string :instructor
      t.string :email_instructor
      t.text :cv
      t.string :institution_instructor
      t.text :resources
      t.text :self_financing
      t.text :scope

      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :programs, [:user_id, :created_at]
  end
end
